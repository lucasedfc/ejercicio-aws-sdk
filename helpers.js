const minAge = 18;
const maxAge = 65;

const calculateAge = (birthDate) => {
    const ageDiff = Date.now() - new Date(birthDate).getTime()
    const ageDiffToDateFormat = new Date(ageDiff);
    return Math.abs((ageDiffToDateFormat.getUTCFullYear() -1970));    
}

const validateParams = (data) => {
    
    if(!data.name || !data.lastName || !data.birthDate || !data.dni) {
       return {
        statusCode: 400,
        body: 'Parameters missing, please complete all parameters!'
       }
    }

    const age = calculateAge(data.birthDate);

    if(age < minAge || age > maxAge) {
        return {
            statusCode: 400,
            body: 'Your age is not allowed for this operation'
            }
    }

    return data;    
}

const itemParams = (data) => {
    const Item = {
        dni: {'S': data.dni},
        firstName:  { 'S': data.firstName },
        lastName: { 'S': data.lastName },
        birthDate: {'S': data.birthDate}
    }

    const params = {
        Item,
        TableName: 'lucasHeim-clients'
    }

    return params;
}

const getSeason = (month) => {
    if (3 <= month && month <= 5)  return 'Sweater' // autumn
    if (6 <= month && month <= 8)  return 'Buzo'   // winter
    if (9 <= month && month <= 12) return'Camisa' // spring    
    return 'Remera'; // summer
}

const giftParams = (data) => {    

    const params = {

        TableName: 'lucasHeim-clients',
        Key: {
            dni: 
                { S: data.dni }
        },
        UpdateExpression: 'set #gift = :gi',
        ExpressionAttributeNames: {
            '#gift': 'gift',
        },
        ExpressionAttributeValues: {
            ':gi': { S: data.gift},
        },
        ReturnValues: "ALL_NEW",       
    }

    return params;
}

    

const cardParams = (data) => {    

    const params = {

        TableName: 'lucasHeim-clients',
        Key: {
            dni: 
                { S: data.dni }
        },
        UpdateExpression: 'set #cardNumber = :cn, #expirationDate = :ex, #securityCode = :sc, #cardType = :ct',
        ExpressionAttributeNames: {
            '#cardNumber': 'cardNumber',
            '#expirationDate': 'expirationDate',
            '#securityCode': 'securityCode',
            '#cardType': 'cardType'
        },
        ExpressionAttributeValues: {
            ':cn': { S: data.cardNumber},
            ':ex': { S: data.expirationDate},
            ':sc': { S: data.cardCode},
            ':ct': { S: data.type}
        },
        ReturnValues: "ALL_NEW",       
    }

    return params;
}

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min); 
}

module.exports = {
    validateParams,
    itemParams,
    calculateAge,
    randomNumber,
    cardParams,
    getSeason,
    giftParams
}