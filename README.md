
# aws-sdk-exercise



1 - create-client.js: Customer registration, required fields: dni, lastName, name, birthDate. The client's age cannot be less than 18 and more than 65 years

    {
        "dni": "12345679",
        "name": "Michael",
        "lastName": "Scott",
        "birthDate": "27/03/1990"
    }    

2 - create-card: Generates a card that is classified in 2 ways, Classic and Gold. For those over 45 years old it is Gold for the others it will be Classic.

    {
        "dni": "12345679"
    }

3 - create-gift: Assign a birthday gift according to the season of the year:

Summer: T-shirt,
Autumn: Sweater,
Winter: jacket,
Spring: Shirt

    {
        "dni": "12345679"
    }
