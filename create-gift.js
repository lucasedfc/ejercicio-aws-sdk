const DYNAMODB = require('aws-sdk/clients/dynamodb');
const { getSeason, giftParams } = require('./helpers');
const clientDynamoDB = new DYNAMODB({
    region: 'us-east-1'
});

exports.handler = async(event) => {

    const data = JSON.parse(event.Records[0].body);
    const body = JSON.parse(data.Message);
    
    const month = new Date(body.birthDate).getMonth();
    const gift = getSeason(month);    
    const dbParams = giftParams({dni: body.dni, gift});
    try {
        await clientDynamoDB.updateItem(dbParams).promise();
    } catch (error) {
        console.error(`Error updating gift: ${error}`);
        return {
            statusCode: 500,
            error
        }
    }

    return {
        statusCode: 200,
        body: 'Gift created!'
    }   

}