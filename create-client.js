// Imports
const DYNAMODB = require('aws-sdk/clients/dynamodb');
const SNS = require('aws-sdk/clients/sns');
const { validateParams, itemParams } = require('./helpers');

// Instance
const dynamodb = new DYNAMODB({region: 'us-east-1'});
const sns = new SNS({region: 'us-east-1'});

exports.handler = async (event) => {
    
    console.log(`Event Data: ${JSON.stringify(event)}`);

    const client = validateParams(event);
    const dbParams = itemParams(client);

    try {
        await dynamodb.putItem(dbParams).promise();
        await sns.publish({
            Message: JSON.stringify(event),
            TopicArn: 'arn:aws:sns:us-east-1:450865910417:lucasHeim-client-created'
        }).promise();
        
    } catch (error) {
        console.error(`Error creating client: ${error}`);
        return {
            statusCode: 500,
            error
        }
    }

    return {
        statusCode: 200,
        body: 'Client Created!'
    }
}