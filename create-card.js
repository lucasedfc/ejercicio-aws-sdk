/* eslint-disable no-octal */
const DYNAMODB = require('aws-sdk/clients/dynamodb');

const dynamodb = new DYNAMODB({
    region: "us-east-1",
  });

const { calculateAge, randomNumber, cardParams } = require('./helpers');
const MIN_CARD_NUM = 1000;
const MAX_CARD_NUM = 9999;
const MIN_CODE_NUM = 100;
const MAX_CODE_NUM = 999;


exports.handler = async (event) => {

    const data = JSON.parse(event.Records[0].body);
    const body = JSON.parse(data.Message);
    const type = calculateAge(body.birthDate) > 45 ? 'Gold' : 'Classic';
    const cardNumber = randomNumber(MIN_CARD_NUM,MAX_CARD_NUM) + '-' +
    randomNumber(MIN_CARD_NUM,MAX_CARD_NUM) + '-' +
    randomNumber(MIN_CARD_NUM,MAX_CARD_NUM) + '-' +
    randomNumber(MIN_CARD_NUM,MAX_CARD_NUM);

    const cardCode = randomNumber(MIN_CODE_NUM, MAX_CODE_NUM);
    const expirationDate = randomNumber(01,12) + '/' + randomNumber(22,30);
    const dbParams = cardParams({dni: body.dni, cardNumber, cardCode, expirationDate, type});

    try {
        const result = await dynamodb.updateItem(dbParams).promise();
        console.info(result);
    } catch (error) {
        console.error(`Error updating item: ${error}`);
        return {
            statusCode: 500,
            error
        }
    }

    return {
        statusCode: 200,
        body: 'Card Created!'
    }
}